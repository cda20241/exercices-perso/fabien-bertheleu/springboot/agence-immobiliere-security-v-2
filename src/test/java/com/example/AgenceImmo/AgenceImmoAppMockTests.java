package com.example.AgenceImmo;

import com.example.AgenceImmo.controller.BienController;
import com.example.AgenceImmo.controller.PersonneController;
import com.example.AgenceImmo.dto.BaseBienDTO;
import com.example.AgenceImmo.dto.PersonneDTO;
import com.example.AgenceImmo.entity.BienEntity;
import com.example.AgenceImmo.entity.PersonneEntity;
import com.example.AgenceImmo.enums.RoleEnum;
import com.example.AgenceImmo.repository.BienRepository;
import com.example.AgenceImmo.repository.PersonneRepository;
import com.example.AgenceImmo.service.BienService;
import com.example.AgenceImmo.service.PersonneService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;

import javax.swing.text.html.parser.Entity;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@SpringBootTest

class AgenceImmoAppMockTests {

    @Mock
    private PersonneRepository personneRepository;
    @Mock
    private BienRepository bienRepository;

    @Mock
    private BienEntity entity;
    @InjectMocks
    private BienController bienController;

    @Mock
    private BienService bienService;
    @InjectMocks
    private PersonneService personneService;


    @InjectMocks
    private PersonneController personneController;


    @Test
    public void test_updatePersonne() {
        // Arrange
        Long id = 1L;
        PersonneEntity existingPersonne = new PersonneEntity();
        existingPersonne.setId(null);
        existingPersonne.setNom("John");
        existingPersonne.setPrenom("Doe");
        existingPersonne.setRue("123 Main St");
        existingPersonne.setVille("New York");
        existingPersonne.setCodePostal(12345);

        PersonneEntity updatedPersonne = new PersonneEntity();
        updatedPersonne.setId(id);
        updatedPersonne.setNom("Jane");
        updatedPersonne.setPrenom("Doe");
        updatedPersonne.setRue("456 Oxford Street");
        updatedPersonne.setVille("Londres");
        updatedPersonne.setCodePostal(67890);

        when(personneRepository.findById(id)).thenReturn(Optional.of(existingPersonne));
        when(personneRepository.save(updatedPersonne)).thenReturn(updatedPersonne);

        // Act
        ResponseEntity<?> response = personneController.updatePersonne(id, updatedPersonne);

        // Assert
        assertEquals(id, updatedPersonne.getId());
    }

    @Test
    @Transactional
    public void testDeleteBien() {
        List <BienEntity> listMock   = new ArrayList<>();
        System.out.println("listMock1");
        System.out.println(listMock);
        // Je créée mes données
        Long id = 1L;
        BienEntity bienTest1 = new BienEntity();
        bienTest1.setId(1L);
        bienTest1.setVille("Londres");
        listMock.add(bienTest1);
        System.out.println("listMock");
        System.out.println(listMock);
        BienEntity result = listMock.get(0);
        System.out.println("result");
        System.out.println(result);
        // je mock la fonction "findById"
        when(bienRepository.findById(id)).thenReturn(Optional.of(bienTest1));

        ResponseEntity<String> reponseJuste = bienController.deleteBien(id);


        assertEquals(HttpStatus.OK, reponseJuste.getStatusCode());
        assertEquals("Ce bien a été supprimé de la base de données", reponseJuste.getBody());
        System.out.println("listMock.size()");
        System.out.println(listMock.size());


        // Test pour valeur fausse
        ResponseEntity<String> reponseFausse = bienController.deleteBien(9L);

        assertEquals(HttpStatus.NOT_FOUND, reponseFausse.getStatusCode());
        assertEquals("Ce bien n'est pas présent dans la base de données", reponseFausse.getBody());
        System.out.println("listMock3");
        System.out.println(listMock);
    }



}