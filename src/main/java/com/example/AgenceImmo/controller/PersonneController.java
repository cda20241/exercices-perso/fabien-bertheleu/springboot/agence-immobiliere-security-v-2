package com.example.AgenceImmo.controller;

import com.example.AgenceImmo.dto.PersonneDTO;
import com.example.AgenceImmo.entity.BienEntity;
import com.example.AgenceImmo.entity.PersonneEntity;
import com.example.AgenceImmo.enums.RoleEnum;
import com.example.AgenceImmo.repository.BienRepository;
import com.example.AgenceImmo.repository.PersonneRepository;
import com.example.AgenceImmo.service.PersonneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/personne")
public class PersonneController {
    @Autowired
    private final PersonneRepository personneRepository;

    @Autowired
    private final PersonneService personneService;

    @Autowired
    private  BienRepository bienRepository;

    public PersonneController(PersonneRepository personneRepository, PersonneService personneService) {
        this.personneRepository = personneRepository;
        this.personneService = personneService;
    }

    @GetMapping("/all")
    public List<PersonneEntity> getAllPersonne() {
        return personneRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getPersonne(@PathVariable Long id) {
        Optional<PersonneEntity> personne = personneRepository.findById(id);
        if (personne.isPresent()) {
            return ResponseEntity.ok(personne.get());
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Cette id ne correspond à aucune personne.");
        }
    }

    @PostMapping("/Ajouter")
    public ResponseEntity<String> addPersonne(@RequestBody PersonneEntity personne) {
        personne.setId(null);
        personne = this.personneRepository.save(personne);
        return ResponseEntity.status(HttpStatus.CREATED).body(personne.getPrenom() + " " + personne.getNom() + " a été ajouté(e) à la base de données.");
    }

    @PutMapping("/Modifier/{id}")
    public ResponseEntity<?> updatePersonne(@PathVariable Long id, @RequestBody PersonneEntity salarie) {
        Optional<PersonneEntity> personneOptional = personneRepository.findById(id);
        if (personneOptional.isPresent()) {
            salarie.setId(id);
            PersonneEntity updatedPersonne = personneRepository.save(salarie);
            return ResponseEntity.ok(updatedPersonne);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Cette id ne correspond à aucune personne.");
        }
    }

    @DeleteMapping("/Effacer/{id}")
    public ResponseEntity<String> deletePersonne(@PathVariable Long id) {
        Optional<PersonneEntity> personne = this.personneRepository.findById(id);
        if (personne.isPresent()) {
            this.personneRepository.deleteById(id);
            return new ResponseEntity<>("Cette personne a été supprimée de la base de données", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Personne non présente dans la base de données", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/filtre/{role}")
    public List<PersonneEntity> filtrePersonne(@PathVariable String role) {
        role = role.toUpperCase();
        return personneRepository.findByRole(RoleEnum.valueOf(role));
    }

    @GetMapping("calcul/{role}")
    public ResponseEntity<String> calculRole(@PathVariable String role) {
        Integer calcul =0;
        switch (role) {
            case "proprietaire":
                calcul = personneRepository.findByRole(RoleEnum.PROPRIETAIRE).size();
                return new ResponseEntity<>(calcul + " " + role + " dans la liste",HttpStatus.OK);
            case "vendeur":
                calcul = (personneRepository.findByRole(RoleEnum.valueOf("VENDEUR"))).size();
                return new ResponseEntity<>(calcul + " " + role + " dans la liste",HttpStatus.OK);
            case "agent":
                calcul = (personneRepository.findByRole(RoleEnum.AGENT)).size();
                return new ResponseEntity<>(calcul + " " + role + " dans la liste",HttpStatus.OK);
            case "prospect":
                calcul = (personneRepository.findByRole(RoleEnum.PROSPECT)).size();
                return new ResponseEntity<>(calcul + " " + role + " dans la liste",HttpStatus.OK);
                default:
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("infoBiensDeLaPersonne/{id}")
    public ResponseEntity<?> trouverBiensParIdPersonne(@PathVariable Long id) {
        Optional<PersonneEntity> personne = personneRepository.findById(id);
        if (personne.isPresent()) {
            return personneService.trouverBiensParIdPersonne(personne.get());
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Cette id ne correspond à aucune personne.");
        }
    }

    @GetMapping("/infos/{id}")
    public PersonneDTO getInfoFiltree (@PathVariable Long id){
            PersonneEntity personne = personneService.getInfoFiltree(id);
        PersonneDTO dto = new PersonneDTO();
        dto.mapper(personne);
        return dto;
     }

    @GetMapping("ajouterIDpersonneIDbien/{id_personne}/{id_bien}")
     public ResponseEntity<String > ajouterPersonne(@PathVariable Long id_personne, @PathVariable Long id_bien) {
        Optional<PersonneEntity> personneOptional = personneRepository.findById(id_personne);
        Optional<BienEntity> bienOptional = bienRepository.findById(id_bien);
        return personneService.ajouterPersonneBien(personneOptional.get().getId(), bienOptional.get().getId());
    }
}