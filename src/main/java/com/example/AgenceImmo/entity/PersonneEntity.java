package com.example.AgenceImmo.entity;

import com.example.AgenceImmo.enums.RoleEnum;
import jakarta.persistence.*;
import lombok.Data;
import lombok.RequiredArgsConstructor;


import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Data
@RequiredArgsConstructor

@Table(name="personne")

public class PersonneEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_personne", nullable = false, updatable = false)
    private Long id;

    @Column
    private String nom;
    @Column
    private String prenom;
    @Column
    private String rue;
    @Column
    private String ville;
    @Column
    private Integer codePostal;

    @Enumerated(EnumType.STRING)
    private RoleEnum role;

    public void setBiens(List<BienEntity> list) {
    }
}