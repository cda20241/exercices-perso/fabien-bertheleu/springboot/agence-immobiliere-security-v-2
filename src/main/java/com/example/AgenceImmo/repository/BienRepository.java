package com.example.AgenceImmo.repository;

import com.example.AgenceImmo.entity.BienEntity;
import com.example.AgenceImmo.entity.PersonneEntity;
import com.example.AgenceImmo.enums.RoleEnum;
import com.example.AgenceImmo.enums.TypeEnum;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BienRepository extends JpaRepository<BienEntity, Long> {
    List<BienEntity> findByType(TypeEnum type);


}
