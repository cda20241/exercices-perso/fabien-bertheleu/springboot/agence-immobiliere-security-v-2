package com.example.AgenceImmo.repository;

import com.example.AgenceImmo.entity.BienEntity;
import com.example.AgenceImmo.entity.PersonneEntity;
import com.example.AgenceImmo.enums.RoleEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import java.util.List;


public interface PersonneRepository extends JpaRepository<PersonneEntity, Long> {
    /**
     * Trouve toutes les personnes ayant le rôle spécifié.
     *
     * @param role Le rôle à rechercher
     * @return La liste des personnes ayant le rôle spécifié
     */
    List<PersonneEntity> findByRole(RoleEnum role);


    @Query(value = "SELECT bien.id_bien, bien.rue, bien.ville, bien.code_postal, bien.type FROM bien " +
            "JOIN personne_bien ON personne_bien.id_bien=bien.id_bien " +
            "JOIN personne ON personne.id_personne=personne_bien.id_personne " +
            "where personne.id_personne = :idPersonne", nativeQuery = true)
    List<BienEntity> trouverBiensParIdPersonne(@Param("idPersonne") Long id);

}
